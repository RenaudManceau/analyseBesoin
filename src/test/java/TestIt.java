import org.junit.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class TestIt {

  // Quand on met 40cts ALORS le café coule et l'argent est encaissé
  @Test
  public void testDrawCafe(){
    Machine machine = new Machine();
    machine.insertMoney(40);
    machine.serveCoffee();
  }
  // Etant donné qu'il n'y a plus de café Quand on met 40cts Alors l'argent est rendu
  @Test
  public void testEmptyCoffee() {
    Machine machine = new Machine();
    machine.setCoffeeAvailable(false);
    machine.insertMoney(40);
  }
  // Etant donné qu'il n'y a plus de gobelets Quand on met 40cts ALORS rends 40cts et pas de café
  // Etant donné qu'il n'y a plus de gobelets et qu'une tasse est détectée Quand on met 40cts ALORS le café coule
  // Etant donné que la machine n'a plus d'eau Quand on met 40cts ALORS l'argent est rendu
  // Etant donné que l'utilisateur a appuyé sur "Sucre" Quand l'utilisateur on met 40cts ALORS le café coule avec le sucre ajouté
  // Etant donné que le café n'a pas coulé et que 40cts ont été insérés Quand le client appuie sur annuler ALORS le paiement est remboursé et aucun café servi

// Trucs un peu propre
  /**
   * QUAND on met 40cts ALORS un café coule
   */
  @Test
  public void testGetCoffeeOK()
  {
    MachineCafe m = new MachineCafe();
    MachineCafeAction action = new MachineCafeAction();

    action.getCoffee(40, m, false);

    assertEquals(m.getNumberOfCoffeesServed(), 1);
  }

  /**
   * ETANT DONNE qu'il n'y a plus de café QUAND on met 40cts ALORS l'argent est rendu
   */
  @Test
  public void testGiveMoneyBackNoCoffee()
  {
    MachineCafe m = new MachineCafe();
    MachineCafeAction action = new MachineCafeAction();
    m.setCoffeeNumber(0);

    action.getCoffee(40, m, false);

    assertEquals(0, m.getNumberOfCoffeesServed());
  }

  /**
   * GIVEN - WHEN - Donner >40cts THEN - Café coule ET on encaisse tout l'argent
   */
  @Test
  public void testGetCoffeeAndMoney()
  {
    MachineCafe m = new MachineCafe();
    MachineCafeAction action = new MachineCafeAction();

    action.getCoffee(40, m, false);

    assertEquals(m.getMoneyCollected(), 40);
    assertEquals(m.getNumberOfCoffeesServed(), 1);
    assertEquals(49, m.getCupNumber());
  }


  /**
  * ETANT DONNE qu'il n'y a plus de gobelets QUAND on met 40cts ALORS rend 40cts et pas de café
  */
  @Test
  public void testGiveMoneyBackNoCup()
  {
    MachineCafe m = new MachineCafe();
    MachineCafeAction action = new MachineCafeAction();
    m.setCupNumber(0);

    action.getCoffee(40, m, false);

    assertEquals(m.getNumberOfCoffeesServed(), 0);
    assertEquals(m.getMoneyCollected(), 0);
  }

  /**
   * ETANT donne qu'il n'y a plus de gobelets ET qu'une tasse est détectée QUAND on met 40cts ALORS le café coule
   */
  @Test
  public void testGetCoffeWithMugNoCup()
  {
    MachineCafe m = new MachineCafe();
    MachineCafeAction action = new MachineCafeAction();
    m.setCupNumber(0);
    m.setMugDetected(true);

    action.getCoffee(40, m, false);

    assertEquals(m.getNumberOfCoffeesServed(), 1);
    assertEquals(m.getMoneyCollected(), 40);
  }

  /**
   * ETANT donné que la machine n'a plus d'eau QUAND on met 40cts ALORS l'argent est rendu
   */
  @Test
  public void testGiveMoneyBackNoWater()
  {
    MachineCafe m = new MachineCafe();
    MachineCafeAction action = new MachineCafeAction();
    m.setWater(0);

    action.getCoffee(40, m, false);

    assertEquals(m.getNumberOfCoffeesServed(), 0);
    assertEquals(m.getMoneyCollected(), 0);
  }

  /**
   * ETANT donné qu'une tasse est détectée QUAND on met 40cts ALORS le café coule ET aucun gobelet n'est consommé
   */
  @Test
  public void testGetCoffeeWhenMug()
  {
    MachineCafe m = new MachineCafe();
    MachineCafeAction action = new MachineCafeAction();
    m.setMugDetected(true);

    action.getCoffee(40, m, false);

    assertEquals(50, m.getCupNumber());
    assertEquals(1, m.getNumberOfCoffeesServed());
  }

  /**
   * Etant donné que l'utilisateur a appuyé sur sucre Quand l'utilisateur met 40 cts  Alors  le café coule avec le sucre ajouté
   */
  @Test
  public void testGetSugar()
  {
    MachineCafe m = new MachineCafe();
    MachineCafeAction action = new MachineCafeAction();

    action.getCoffee(40, m, true);

    assertEquals(true, m.isSugar());
  }

  /**
   * QUAND le technicien remet <x> cup ALORS on ajoute <x> stock de cup
   */

  @Test
  public void addCup(){
    MachineCafe m = new MachineCafe();
    MachineCafeAction action = new MachineCafeAction();

    action.cupDown(m);
    action.cupDown(m);
    action.cupDown(m);
    action.cupDown(m);
    action.cupDown(m);
    action.addCup(5, m);
    
    assertEquals(50, m.getCupNumber());
  }

  /**
   *QUAND le technicien remet <x> café ALORS on ajoute <x> stock de café
   */
@Test
  public void addCoffee(){
    MachineCafe m = new MachineCafe();
    MachineCafeAction action = new MachineCafeAction();

    action.coffeeDown(m);
    action.coffeeDown(m);
    action.coffeeDown(m);

    action.addCoffee(3,m);

    assertEquals(30, m.getCoffeeNumber());
  }



  // 2eme piece le café coule
  // 3eme piece le café coule et on encaisse tout l'argent
  // 4eme piece le café coule et on encaisse tout l'argent et on enlève une tasse
  public void testInsertCoin()
  {
    MachineCafe m = new MachineCafe();
    MachineCafeAction action = new MachineCafeAction();

    action.insertCoin(2, m);
    action.insertCoin(4, m);
    action.insertCoin(5, m);
    action.insertCoin(6, m);

    assertEquals(0, m.getActualMoney());
    assertEquals(0, m.getInsertedCoins());

    action.insertCoin(1, m);
    action.insertCoin(50, m);
    action.insertCoin(50, m);
    action.insertCoin(50, m);

    assertEquals(151, m.getActualMoney());
    assertEquals(4, m.getInsertedCoins());
  }

  /**
   * QUAND on met 40 cts ET 50cts ALORS deux cafés coulent et 90cts sont encaissés.
   */
  @Test
  public void testInsertTooMuchMoney(){
    //Initialisation
    MachineCafe m = new MachineCafe();
    MachineCafeAction action = new MachineCafeAction();

    action.getCoffee(90,m,false);


    //Checker si deux cafés ont coulés et que 90 cts ont bien été encaissé
    assertEquals(2,m.getNumberOfCoffeesServed());
    assertEquals(90, m.getMoneyCollected());
    assertEquals(1090,m.getActualMoney());
  }

  /**
   * QUAND le total de <x> pièces est supérieur à 40 centimes ALORS la machine fait couler un café et encaisse la monnaie CAS 1 ... 4
   */
  @Test
  public void compterSupQuarantCts(){

    MachineCafe m = new MachineCafe();
    MachineCafeAction action = new MachineCafeAction();
    int moneyInserted = m.getActualMoney();
    int nbInsertedCoins = m.getInsertedCoins();

    if (nbInsertedCoins == 1 && moneyInserted > 40) {
      m.setInsertedCoins(nbInsertedCoins);
      action.getCoffee(40, m,false);
      assertEquals(40,m.getMoneyCollected());
      assertEquals(nbInsertedCoins,m.getInsertedCoins());
      assertEquals(1,m.getNumberOfCoffeesServed());

    } else if (nbInsertedCoins == 2 && moneyInserted > 40) {

      m.setInsertedCoins(nbInsertedCoins);
      action.getCoffee(40, m,false);
      assertEquals(40,m.getMoneyCollected());
      assertEquals(nbInsertedCoins,m.getInsertedCoins());
      assertEquals(1,m.getNumberOfCoffeesServed());
    }
    else if (nbInsertedCoins == 3 && moneyInserted > 40) {

      m.setInsertedCoins(nbInsertedCoins);

      action.getCoffee(40, m,false);

      assertEquals(40,m.getMoneyCollected());
      assertEquals(nbInsertedCoins,m.getInsertedCoins());
      assertEquals(1,m.getNumberOfCoffeesServed());
    }
    else if (nbInsertedCoins == 4 && moneyInserted > 40) {
      m.setInsertedCoins(nbInsertedCoins);

      action.getCoffee(40, m,false);

      assertEquals(40,m.getMoneyCollected());
      assertEquals(nbInsertedCoins,m.getInsertedCoins());
      assertEquals(1,m.getNumberOfCoffeesServed());
    }
    else if (nbInsertedCoins > 4 && moneyInserted > 40) {
      action.giveBackMoney(moneyInserted, m);
    }

  }
}