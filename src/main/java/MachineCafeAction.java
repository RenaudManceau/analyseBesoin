public class MachineCafeAction {
    public void getCoffee(int cts, MachineCafe m, boolean isSugarPressed){

        m.setActualMoney(m.getActualMoney()+cts);
        m.setMoneyCollected(m.getMoneyCollected()+cts);
        for(int i =cts;i>=40;i = i-40){

            if(m.getCoffeeNumber() > 0 && (m.getCupNumber() > 0 || m.isMugDetected()) && m.getWater() > 0)
            {
                m.setNumberOfCoffeesServed(m.getNumberOfCoffeesServed() + 1);
                if(!m.isMugDetected())
                {
                    m.setCupNumber(m.getCupNumber() - 1);
                }

                if (isSugarPressed) {
                    m.setSugar(true);
                }
                cts = cts-40;
            } else
            {
                giveBackMoney(cts, m);

            }
        }

    }

    public void giveBackMoney(int cts, MachineCafe m)
    {
        int actualMoney = m.getActualMoney();
        System.out.println("Les " + cts + " centimes sont rendus");

        m.setMoneyCollected(m.getMoneyCollected()-cts);
        m.setActualMoney(actualMoney - cts);
        m.setInsertedCoins(0);
    }

    public void insertCoin(int cts, MachineCafe m)
    {
        int moneyInserted = m.getActualMoney();
        int nbInsertedCoins = m.getInsertedCoins();

        if (nbInsertedCoins == 4 && moneyInserted < 40) {
            giveBackMoney(moneyInserted, m);
        } else {
            m.setActualMoney(m.getActualMoney() + cts);
            m.setInsertedCoins(m.getInsertedCoins() + 1);
        }
    }

    public void addCup(int nbrCup, MachineCafe machineCafe){
        machineCafe.setCupNumber(machineCafe.getCupNumber()+ nbrCup);
    }
    public void addCoffee(int nbrCoffee, MachineCafe machineCafe){

        int nbrCoffeeDoses = machineCafe.getCoffeeNumber();

        for (int i = 0; i < nbrCoffee; i++) {
            if (nbrCoffeeDoses == 30) {
                throw new RuntimeException("La machine est déjà pleine de café");
            }
            machineCafe.setCoffeeNumber(nbrCoffeeDoses + 1);
            nbrCoffeeDoses++;
        }

    }

    public void cupDown(MachineCafe m) {
        m.setCupNumber(m.getCupNumber() - 1);
    }
    public void coffeeDown(MachineCafe m) {
        m.setCoffeeNumber(m.getCoffeeNumber() - 1);
    }

}
